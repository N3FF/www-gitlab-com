---
layout: handbook-page-toc
title: Programs Calendar - People Managers
description: >-
  The Talent Programs Calendar helps people managers priortise and plan ahead. It is the one overview that shows all Talent Programs that require people manager input and execution. 
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## GitLab People Managers Calendar 

This page shows an overview of the Talent Programs that People Managers are involved in. The goal of this page is to ensure that People Managers can plan ahead and have an overview of upcoming programs and timelines.

Each program has its own page with exact dates as SSOT and content that is linked in the calendar `Actions` section below. Exact People program dates may shift slightly as we get closer to the execution of the programs listed, so we recommend you reference the SSOT links for the most accurate timelines and updates. All programs which require people manager action will be announced in Slack in the #managers or #people-managers-and-above channels at the time of launch.

## Communication

Each quarter a Slack reminder will go out in #people-manager-and-above channel to review the next quarter calendar events. Each month shows `Task`, which outlines the people manager responsibility associated with the specific program, and (if applicable) highlights the `Key Date`. We will also leverage our Manager Newsletter and the Company Newsletter for broadcasting our programs widely. 

### FY24 Q2

| **Action**                             | **May**                                | **June**                         | **July**           |
|:--------------------------------------:|:--------------------------------------:|:--------------------------------------:|:--------------------------------------:|
| **[OKR Planning & Execution](https://about.gitlab.com/company/okrs/)**                              | Task: Track Q2 OKR progress | Task: Track Q2 OKR progress                                | Task: Score Q2 OKRs by 2023-07-31 and submit Q3 OKRs by 2023-07-10   |
| **[Promotion Planning, Calibration & Communication](https://about.gitlab.com/handbook/people-group/promotions-transfers/#quarterly-promotion-calibration-process--timeline)**          | Task: Sr Dir+ Calibration at e-group offsite <br> Task: Communicate any approved promotions to team members <br> Key Date: Effective date of promotions 2023-05-01<br>  |                                  | Task: Plan Promotions for Q3 and Calibrate with Leadership by 2023-07-15      |
| **[GitLab Engagement Survey](https://about.gitlab.com/handbook/people-group/engagement/)**                  | Key Date: 2023-05-22 Kick Off                    | Task: Review Results                  | Task: Execute actions following survey   |
| **[Talent Assessment](https://about.gitlab.com/handbook/people-group/talent-assessment/)** |                            |Task: Start Mid-year Check in review with your team, Key Date: Kick off Mid Year Check in 2023-06-15 (TBC)  | Task: Complete your Mid-year check in review                   | 
| **[Career Development Conversations](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/)**          |                                        |                                  |                    |

### FY24 Q3

| **Action**                                    | **August**                   | **September** | **October**        |
|:---------------------------------------------:|:----------------------------:|:-------------:|:------------------:|
| **[OKR Planning & Execution](https://about.gitlab.com/company/okrs/)** | Task: Track Q3 OKR progress | Task: Track Q3 OKR progress                                | Task: Score Q3 OKRs by 2023-10-31 and submit Q3 OKRs by 2023-10-09   |
| **[Promotion Planning, Calibration & Communication](https://about.gitlab.com/handbook/people-group/promotions-transfers/#quarterly-promotion-calibration-process--timeline)**          | Task: Communicate any approved promotions to team members, Key Date: Effective date of promotions 2023-08-01  |                                  |                          |
| **[GitLab Engagement Survey](https://about.gitlab.com/handbook/people-group/engagement/)**                  | Task: Actions incorporated in OKRs, Task: Engagement Survey Action Planning execution    | Task: Engagement Survey Action Planning execution               | Task: Engagement Survey Action Planning execution    | 
| **[Talent Assessment](https://about.gitlab.com/handbook/people-group/talent-assessment/)** |   Task: Mid-year Check in conversation with your team, Key Date: Mid-year check in closes by 2023-08-xx |                          |                     |
| **[Career Development Conversations](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/)**          | Task: Share IGP format with your team members, Key Date: Kick off in August           | Task: Providing continued support on IGP actions of your team members     | Task: Providing continued support on IGP actions of your team members                     |


### FY24 Q4

| **Action**                                    | **November**                | **December**        | **January**                            |
|:---------------------------------------------:|:---------------------------:|:-------------------:|:-------------------:|
| **[OKR Planning & Execution](https://about.gitlab.com/company/okrs/)** | Task: Track Q4 OKR progress | Task: Track Q4 OKR progress                                | Task: Score Q4 OKRs by 2024-01-31 and submit FY25-Q1 OKRs by 2024-01-08   | 
| **[Promotion Planning, Calibration & Communication](https://about.gitlab.com/handbook/people-group/promotions-transfers/#quarterly-promotion-calibration-process--timeline)**          |                  | 
| **[Promotion Effective date & Communication](https://about.gitlab.com/handbook/people-group/promotions-transfers/#quarterly-promotion-calibration-process--timeline)**  |                             |                     | Task: Plan Promotions for Q1 and Calibrate with Leadership by 2024-01-15 |
| **[Talent Assessment](https://about.gitlab.com/handbook/people-group/talent-assessment/)**                         | Task: Write Talent Assessment review for your team members | Task: Calibrate ratings with leadership | Task: Talent Assessment (Review) Discussions with your team |
| **[Annual Comp Calibration](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-review-cycle/)**                   |Task: Add in recommendations for compensation changes for your team, Task: Calibrate the recommendations with your leader, Key date: Final recommendation to be entered in Workday by 2024-01-15 (tbc)                    |
